import pandas
from pandas.plotting  import table
import numpy as np
from sklearn.metrics import roc_auc_score
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import auc
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import os.path
organism = "Dros"
ontologies = {"CC": "Cellular Components", "MF": "Molecular Function"}
algorithms = ["ML.PTRON", "MY.PTRON.OVR", "PTRON.OVR", "POLY.PTRON.OVR", "GAUSS.PTRON.OVR"]
data_folder = "data"

p = pandas.DataFrame(index=algorithms, columns=["roc", "prc"])

prc_roc_scores = dict()
# carica i dati per ogni ontologia
for ontology in ontologies:
    # per ogni algoritmo recupera l'AUROC e AUPRC
    for alg in algorithms:
        stats = pandas.DataFrame.from_csv(
            os.path.join(data_folder, "{}.{}.{}.stats.txt".format(organism, ontology, alg)))
        stats.set_index([[alg]], inplace=True)
        if not (ontology in prc_roc_scores):
            prc_roc_scores[ontology] = pandas.DataFrame(index=algorithms, columns=stats.columns.values, dtype=np.float32)
        prc_roc_scores[ontology].update(stats)

    rng = range(len(algorithms))
    #disegna il grafico
    df = prc_roc_scores[ontology][["f-score", "precision", "recall", "roc", "prc"]]
    df.round(2).to_csv(os.path.join(data_folder,"{}.results.txt".format(ontology)))
    df.plot.bar()
    plt.ylim(ymax=1, ymin=0)
    plt.title(ontologies[ontology])
    plt.show()
