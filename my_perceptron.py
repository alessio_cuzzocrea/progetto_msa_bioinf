import time

import numpy as np
from sklearn import datasets
from sklearn.base import BaseEstimator
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_score
from sklearn.utils.extmath import safe_sparse_dot

class Perceptron(BaseEstimator):
    def __init__(self, n_epochs=5):
        self.n_epochs = n_epochs
        self.weights = None

    def decision_function(self, X):
        # simple predict one instance
        if self.weights is None:
            raise Exception("weights not trained")
        preds = list()
        return safe_sparse_dot(X, np.transpose(self.weights))
        # for i, row in enumerate(X):
            # scalar = np.dot(row, self.weights)
            # preds.append(scalar)
        # return np.asarray(preds)

    def predict(self, X):
        # simple predict one instance
        if self.weights is None:
            raise Exception("weights not trained")
        preds = list()
        for i, row in enumerate(X):
            scalar = np.dot(row, self.weights)
            preds.append(1 if scalar >= 0 else 0)
        return np.asarray(preds)

    def fit(self, X, y):
        self.weights = np.zeros(len(X[0]))
        for epoch in range(self.n_epochs):
            sum_error = 0.0
            for i, row in enumerate(X):
                signum = -1 if y[i] == 0 else y[i]
                product = np.dot(row, self.weights)
                product = product * signum
                if product <= 0:
                    self.weights = (signum * row) + self.weights
        return self.weights


class PolynomialPerceptron(BaseEstimator):
    def __init__(self, n=3):
        self.mistakes = list()
        self.training_data = None
        self.training_labels = None
        self.n = n

    def decision_function(self, X):
        preds = list()
        for i, row in enumerate(X):
            X_s = self.training_data[np.asarray(self.mistakes)]
            y_s = self.training_labels[np.asarray(self.mistakes)]
            x_t = row.transpose()
            y_s[y_s == 0] = -1
            dot_product = np.dot(X_s, x_t)
            dot_product = dot_product + 1
            kernel = np.power(dot_product, self.n)
            kernel = np.multiply(kernel, y_s)
            preds.append(kernel.sum())
        return np.asarray(preds)

    def fit(self, X, y):
        self.training_data = X
        self.training_labels = y
        self.mistakes = list()
        for i, row in enumerate(X):
            target = -1 if y[i] == 0 else y[i]
            kernel = np.zeros(len(row))
            if len(self.mistakes) != 0:
                X_s = X[np.asarray(self.mistakes)]
                y_s = y[np.asarray(self.mistakes)]
                x_t = row.transpose()
                y_s[y_s == 0] = -1
                dot_product = np.dot(X_s, x_t)
                dot_product = dot_product + 1
                kernel = np.power(dot_product, self.n)
                kernel = np.multiply(kernel, y_s)
            prediction = -1 if np.sum(kernel) <= 0.0 else 1
            if prediction != target:
                self.mistakes.append(i)

    def predict(self, X):
        preds = list()
        for i, row in enumerate(X):
            X_s = self.training_data[np.asarray(self.mistakes)]
            y_s = self.training_labels[np.asarray(self.mistakes)]
            x_t = row.transpose()
            y_s[y_s == 0] = -1
            dot_product = np.dot(X_s, x_t)
            dot_product = dot_product + 1
            kernel = np.power(dot_product, self.n)
            kernel = np.multiply(kernel, y_s)
            preds.append(0 if kernel.sum() <= 0.0 else 1)
        return np.asarray(preds)


class GaussianPerceptron(BaseEstimator):
    def __init__(self, gamma=2.0):
        self.mistakes = list()
        self.training_data = None
        self.training_labels = None
        self.gamma = gamma

    def decision_function(self, X):
        preds = list()
        for i, row in enumerate(X):
            X_s = self.training_data[np.asarray(self.mistakes)]
            y_s = self.training_labels[np.asarray(self.mistakes)]
            x_t = row
            y_s[y_s == 0] = -1
            difference = np.subtract(X_s, x_t)
            difference = np.linalg.norm(difference, 2, 1)
            difference = np.power(difference, 2)
            difference = difference * -(1 / (2 * self.gamma))
            kernel = np.exp(difference)
            preds.append(kernel.sum())
        return np.asarray(preds)

    def fit(self, X, y):
        self.training_data = X
        self.training_labels = y
        self.mistakes = list()
        for i, row in enumerate(X):
            target = -1 if y[i] == 0 else y[i]
            kernel = np.zeros(1)
            if len(self.mistakes) != 0:
                X_s = X[np.asarray(self.mistakes)]
                y_s = y[np.asarray(self.mistakes)]
                x_t = row
                y_s[y_s == 0] = -1
                difference = np.subtract(X_s, x_t)
                difference = np.linalg.norm(difference, 2, 1)
                difference = np.power(difference, 2)
                difference = difference * -(1/(2*self.gamma))
                kernel = np.exp(difference)
                kernel = np.multiply(kernel, y_s)
            prediction = -1 if np.sum(kernel) <= 0.0 else 1
            if prediction != target:
                self.mistakes.append(i)

    def predict(self, X):
        preds = list()
        for i, row in enumerate(X):
            X_s = self.training_data[np.asarray(self.mistakes)]
            y_s = self.training_labels[np.asarray(self.mistakes)]
            x_t = row
            y_s[y_s == 0] = -1
            difference = np.subtract(X_s, x_t)
            difference = np.linalg.norm(difference, 2, 1)
            difference = np.power(difference,2)
            difference = difference * -(1 / (2 * self.gamma))
            kernel = np.exp(difference)
            preds.append(0 if kernel.sum() <= 0.0 else 1)
        return np.asarray(preds)


def main():
    X, y = datasets.load_breast_cancer(return_X_y=True)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25)

    array = np.random.rand(3000)
    array = np.exp(array)
    array = np.exp(array)
    array = np.exp(array)
    print(len(array))
    print(array)
    start_time = time.clock()
    array.sum()
    print("sum time: %s" % (time.clock() - start_time))
    exit(4)
    gp = GaussianPerceptron()
    kp = PolynomialPerceptron()
    gp.fit(X_train, y_train)
    kp.fit(X_train, y_train)
    p = Perceptron()
    p.fit(X_train, y_train)
    print("Gaussian Perceptron score: ", precision_score(y_test, gp.predict(X_test)))
    print("Kernel Perceptron score: ", precision_score(y_test, kp.predict(X_test)))
    print("Perceptron score: ", precision_score(y_test, p.predict(X_test)))
if __name__ == "__main__":
    main()
