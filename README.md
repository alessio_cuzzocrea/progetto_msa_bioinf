# Predizione della funzione proteica #

Scopo di questo progetto � quello di verificare il comportamento di vari algoritmi di apprendimento supervisionato su dei dati relativi alle proteine del moscerino della frutta.  
Sono stati utlizzati i seguenti algoritmi:  
1) Percettrone  
2) Percettrone con Kernel (Gaussiano e polinomiale)  
3) Percettrone multistrato.  

Per confrontare gli algoritmi sono state utilizzate le seguenti metriche:  
1) ROC Curve  
2) PRC Curve  
3) Multilabel Precision  
4) Multilabel Recall  

Per ulteriori dettagli � disponibile una relazione all'interno del repository.