import numpy as np
from sklearn.metrics import roc_auc_score, precision_recall_curve, auc


"""
    y_target(pandas.DataFrame): etichette vere
    y_predicted(pandas.DataFrame): etichette predette
    :returns 3 pandas.Series contententi precision, recall, f_measure per ogni esempio(riga)
"""


def f_measure_multilabel(y_target, y_predicted, verbose=True):
    support_df = y_predicted.add(y_target)
    #calcolo dei TP,TN,FP,FN
    #riempi con 0 eventuali valori NaN
    true_positives = support_df.apply(lambda x: len(x.values[x.values == 2]), axis=1).astype(np.int32)
    true_positives.fillna(0, inplace=True)
    true_negatives = support_df.apply(lambda x: len(x.values[x.values == 0]), axis=1).astype(np.int32)
    true_negatives.fillna(0, inplace=True)
    support_df = y_predicted.subtract(y_target)
    false_positives = support_df.apply(lambda x: len(x.values[x.values == 1]), axis=1).astype(np.int32)
    false_positives.fillna(0, inplace=True)
    false_negatives = support_df.apply(lambda x: len(x.values[x.values == -1]), axis=1).astype(np.int32)
    false_negatives.fillna(0, inplace=True)

    tot_sum = true_positives.values.sum() + true_negatives.values.sum() + false_negatives.values.sum() + false_positives.values.sum()
    #check sulla correttezza delle somme
    if tot_sum != y_target.size:
        print("i valori non corrispondono")
        print("sum: %d, tp_count: %d, tn_count: %d, fn_count: %d, fp_count: %d" % (y_target.size, true_positives.values.sum(),
                                                                                   true_negatives.values.sum(),
                                                                                   false_negatives.values.sum(),
                                                                                   false_positives.values.sum()))
        raise Exception
    sum_tp_fp = true_positives.add(false_positives)
    sum_tp_fp.replace(to_replace=0, value=1, inplace=True)
    sum_tp_fn = true_positives.add(false_negatives)
    sum_tp_fn.replace(to_replace=0, value=1, inplace=True)

    precision = true_positives.div(sum_tp_fp)
    recall = true_positives.div(sum_tp_fn)
    prec_recall = precision.add(recall)
    prec_recall.replace(to_replace=0, value=1, inplace=True)
    f_measure = (precision.multiply(recall).multiply(2)).div(prec_recall)
    if verbose:
        print("sum: %d, tp_count: %d, tn_count: %d, fn_count: %d, fp_count: %d" % (tot_sum, true_positives.values.sum(),
                                                                                   true_negatives.values.sum(),
                                                                                   false_negatives.values.sum(),
                                                                                   false_positives.values.sum()))
        avg_precision = np.sum(precision) / precision.size
        avg_f_measure = np.sum(f_measure) / f_measure.size
        avg_recall = np.sum(recall) / recall.size
        print("avg precision %f, avg f measure %f, avg recall %f" % (avg_precision, avg_f_measure, avg_recall))
    return precision, recall, f_measure


"""
    y_target(pandas.DataFrame)
    y_scores(pandas.DataFrame): soglie di probabilità o punteggi (predict_proba o decision_function)
    round_digits: fattore di arrotondamento
    :returns miglior f-score, precisione e recall medi e relativa soglia 
"""


def find_max_f_score_per_example(y_target, y_scores, round_digits=3, verbose=True):
    # seleziona le righe con almeno un 1
    scores = y_scores[(y_target.T != 0).any()]
    targets = y_target[(y_target.T != 0).any()]
    scores = scores / np.max(scores.values)
    #arrotonda scores, seleziona i valori unici e ordinalo in maniera discendente
    thresholds = np.sort(np.unique(scores.round(round_digits).values))[::-1]
    best_f = -1
    best_threshold = None
    best_prec = None
    best_rec = None
    prec = rec = 0

    for t in thresholds:
        predicted = scores.applymap(lambda x: 1 if x >= t else 0)
        # calcola l'f_score per questa soglia
        if(verbose):
            print("current threshold:", t)
        prec, rec, f_score = f_measure_multilabel(targets, predicted, verbose=verbose)
        current_mean = f_score.mean()
        if best_f < current_mean:
            best_f = current_mean
            best_threshold = t
            best_rec = rec.mean()
            best_prec = prec.mean()
    return best_f, best_threshold, best_prec, best_rec

"""
    labels: etichette vere
    scores: punteggi(o probabilità) dati dai predittori, ottenibili con le funzioni decision_function o predict_proba
    returns: coppia contenente la media AUROC e AUPRC
"""

def compute_auroc_auprc(labels, scores):
    roc = list()
    prc = list()
    for label in labels.columns:
        roc.append(roc_auc_score(labels.get(label), scores.get(label)))
        p, r, _ = precision_recall_curve(labels.get(label), scores.get(label))
        prc.append(auc(r, p))
    # compute macro-avg au_roc e au_prc
    return np.mean(roc), np.mean(prc)