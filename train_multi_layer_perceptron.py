import time

from sklearn.metrics import roc_curve, roc_auc_score, precision_recall_curve
from sklearn.model_selection import StratifiedKFold, train_test_split, KFold
import numpy as np
import pandas
import os.path
from sklearn.linear_model import Perceptron
from sklearn.multiclass import OneVsRestClassifier
from sklearn.neural_network import MLPClassifier

from helpers import f_measure_multilabel, find_max_f_score_per_example, compute_auroc_auprc
import my_perceptron
#nome organismo e onotologia
organism = "Dros"
ontologies = ["CC", "MF"]
#nome algoritmo
algorithm = "ML.PTRON"
data_folder = "data"
adj_matrix = pandas.read_table(os.path.join(data_folder,"{}.adjmatrix.txt".format(organism)),
                            sep='\t', header=0, index_col=0).astype(np.float32)

for ontology in ontologies:
    stats_file = "{}.{}.{}.stats.txt".format(organism, ontology, algorithm)
    layers_file = "{}.{}.{}.layers.txt".format(organism, ontology, algorithm)
    labels = pandas.read_table(os.path.join(data_folder, "{}.{}.ann.txt".format(organism, ontology)),
                               sep='\t', header=0, index_col=0)
    if os.path.isfile(os.path.join(data_folder,stats_file)):
        print("dati già computati per l'organismo %s con algoritmo %s per l'ontologia %s" % (organism, algorithm, ontology))
    else:
        print("computo i dati per l'organismo %s con algoritmo %s per l'ontologia %s" % (
        organism, algorithm, ontology))
        best_f = -1
        for layers in [(100,100), (200,200), (300,300), (300,300,300)]:
            classifier = MLPClassifier(hidden_layer_sizes=(layers), solver="sgd")
            k_folds = KFold(n_splits=5, shuffle=True)
            k_split = k_folds.split(adj_matrix.values, labels.values)
            f_list = list()
            t_list = list()
            p_list = list()
            r_list = list()
            start_time = time.clock()
            scores = pandas.DataFrame(index=adj_matrix.index, columns=labels.columns.values, dtype=np.float32)
            for k, (train_idx, test_idx) in enumerate(k_split):
                classifier.fit(adj_matrix.values[train_idx], labels.values[train_idx])
                predicted_scores = classifier.predict_proba(adj_matrix.values[test_idx])
                df_scores = pandas.DataFrame(data=predicted_scores, index=adj_matrix.index[test_idx],
                                      columns=labels.columns.values, dtype=np.float32)
                df_test_labels = pandas.DataFrame(data=labels.values[test_idx], index=adj_matrix.index[test_idx],
                                      columns=labels.columns.values, dtype=np.float32)
                scores.update(df_scores, raise_conflict=True)
                f, t, p, r = find_max_f_score_per_example(df_test_labels, df_scores, round_digits=2, verbose=False)
                f_list.append(f)
                t_list.append(t)
                p_list.append(p)
                r_list.append(r)

            print("time elapsed: %s, n: %s" % ((time.clock() - start_time), (layers,)))
            if np.mean(f_list) > best_f:
                best_f = np.mean(f_list)
                best_layers = layers
                best_p = np.mean(p_list)
                best_r = np.mean(r_list)
                best_t = np.mean(t_list)
                best_roc, best_prc = compute_auroc_auprc(labels, scores)

        stats = pandas.DataFrame(
            data={"f-score": best_f, "precision": best_p, "recall": best_r, "threshold": best_t,
                  "roc": best_roc, "prc": best_prc}, index=[0])
        stats.to_csv(os.path.join(data_folder,stats_file))
        layers = pandas.Series(best_layers)
        layers.to_csv(os.path.join(data_folder,layers_file))
        print("stats")
        print(stats)
        print("layers", best_layers)


exit(0)
